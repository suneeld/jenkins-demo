package com.example.dumm_maven_proj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DummMavenProjApplication {

    public static void main(String[] args) {
        SpringApplication.run(DummMavenProjApplication.class, args);
    }

}
